const loader = document.querySelector('.loader-holder');
let commentsHolder = document.querySelector('.comments-holder');
let responseTime = `Підбірка коментарів станом на ${new Date().getFullYear()}-${new Date().getMonth() < 10?`0${new Date().getMonth()+1}`: new Date().getMonth()+1}-${new Date().getDate() < 10? `0${new Date().getDate()+1}`:new Date().getDate()}`;
const switcher = document.querySelector('.switcher');
let start = 0;
let end = 10;
// document.getElementById('left').disabled = 'true';
if(start === 0){
	document.getElementById('left').disabled = 'true';
}

//  формування запита на сервер
function getServer(url, callback = () => {}) {
    loader.classList.add("show")
    const ajax = new XMLHttpRequest();
    ajax.open("get", url);
    ajax.send();
    ajax.addEventListener("readystatechange", () => {
        if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
            callback(JSON.parse(ajax.response));
            loader.classList.remove("show");
        } else if (ajax.readyState === 4) {
            throw new Error(`Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`)
        }
    })
}
    //  функція по формуванню і виводу коментарів
function commentShow(comments = []){
comments.forEach(({id, name, email, body}) =>{
	if(id > start && id <= end){
	let card = document.createElement('div');
	card.classList.add('card');
	commentsHolder.append(card);

	const commentIncludes = `
	<span>${id}.</span>
	<span class='comment-name'>Ім'я: ${name}</span><br/><br/>
	<span>Комертар: ${body}</span><br/>
	<span><a href="mailto:${email}">Відповісти</a></span><br/>`
	card.insertAdjacentHTML('beforeend', commentIncludes);	
}
})

}
    // здійснення запита (перший, на загрузку сторінки)
getServer('https://jsonplaceholder.typicode.com/comments', commentShow);

// подія натиску на кнопку зміни коментарів
switcher.addEventListener('click', (e) =>{
	[...document.querySelectorAll('.card')].forEach((card) => {card.remove()}) /* прибираємо попередньо завантажені картки */
	if(e.target.id === 'left'){
	end = start;
	start = start - 10;
	console.log(start, end);
	end !== 500 ? document.getElementById('right').disabled = false:	document.getElementById('right').disabled = true;
	start !== 0 ? document.getElementById('left').disabled = false:	document.getElementById('left').disabled = true;
	} else if(e.target.id === 'right'){
		start = end;
		end = end + 10;
		console.log(start, end);
		start !== 0 ? document.getElementById('left').disabled = false:	document.getElementById('left').disabled = true;
		end !== 500 ? document.getElementById('right').disabled = false: document.getElementById('right').disabled = true;

	}

	//   здійснення запиту і виводу коментарів після натискання кнопок зміни коментарів
	getServer('https://jsonplaceholder.typicode.com/comments', commentShow);
	})
	console.log(start, end);

	
