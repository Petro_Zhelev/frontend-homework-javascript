const loader = document.querySelector('.loader-holder');

function getServer(url, callback = () => {}) {
    loader.classList.add("show")
    const ajax = new XMLHttpRequest();
    ajax.open("get", url);
    ajax.send();
    ajax.addEventListener("readystatechange", () => {
        if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
            callback(JSON.parse(ajax.response));
            loader.classList.remove("show");
        } else if (ajax.readyState === 4) {
            throw new Error(`Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`)
        }
    })
}

function heroCardCreate(data = []){
	data.results.forEach(({name, gender, height, skin_color, birth_year, homeworld}) => {
	let card = document.createElement('div');
	card.classList.add('card');
	document.body.append(card);
	
		const heroCard = `
		<span >Ім'я: <span class="hero-name">${name}</span></span><br/>
		<span>Стать: ${gender}</span><br/>
		<span>Зріст: ${height}</span><br/>
		<span>Колір шкіри: ${skin_color}</span><br/>
		<span>Рік народження: ${birth_year}</span><br/>
		<span>Планета на якій народився: <a href="${homeworld}">${homeworld}</a></span><br/>
		`
		card.insertAdjacentHTML('beforeend', heroCard)
	})
}


getServer('https://swapi.dev/api/people', heroCardCreate);
