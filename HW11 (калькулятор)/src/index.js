/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/
window.addEventListener('DOMContentLoaded', () => {

	const enteredData = {
		num1: '',
		num2: '',
		sign: '',
		memory: '',
	};

	const res = (num1, sign, num2) => {
		if (sign === '+') { return Number(num1) + Number(num2) }
		else if (sign === '-') { return num1 - num2 }
		else if (sign === '*') { return num1 * num2 }
		else if (sign === '/') { return num1 / num2 }
	};

   
    
	// звертаємося до елементів
	const equal = document.getElementsByClassName('orange')[0]; // кнопка "="
	const display = document.querySelector('.display > input'); // поле вводу 
	 //  створюємо 'm' для дисплею
	 const memoryM = document.createElement('div');
	 memoryM.id = 'm';
	 memoryM.textContent = 'm';
	 document.querySelector('.display').append(memoryM);

	const validate = (p, v) => p.test(v);
	const show = (info) => {
		display.value = info;
	};
	document.querySelector('.keys').addEventListener('click', (e) => {
		if (validate(/[\d\.]/, e.target.value) && enteredData.sign === '') {
			enteredData.num1 += e.target.value;
			show(enteredData.num1);
		} else if (e.target.value === 'm+') {
			enteredData.memory = display.value;
			memoryM.style = 'display: block';
		} else if (e.target.value === 'm-') {
			enteredData.memory = -display.value;
			memoryM.style = 'display: block';
		} else if (e.target.value === 'mrc' && enteredData.memory !== '' && display.value === String(enteredData.memory)) {
			enteredData.memory = '';
			memoryM.style = 'display: none';
		} else if (e.target.value === 'mrc' && enteredData.memory !== '' && enteredData.num1 === '') {
			display.value = enteredData.memory;
			enteredData.num1 = enteredData.memory;
			enteredData.num2 = '';
			enteredData.sign = '';
			memoryM.style = 'display: block';
		} else if (e.target.value === 'mrc' && enteredData.memory !== '' && enteredData.num1 !== '') {
			display.value = enteredData.memory;
			enteredData.num2 = enteredData.memory;
			memoryM.style = 'display: block';
		}else if (validate(/[/+\-*]/, e.target.value) && enteredData.num1 !== '' && enteredData.num2 !== '' && enteredData.sign !== ''){
			show(res(enteredData.num1, enteredData.sign, enteredData.num2));
			enteredData.num1 = res(enteredData.num1, enteredData.sign, enteredData.num2);
			enteredData.num2 ='';
			enteredData.sign = e.target.value;
		}  else if (validate(/[/+\-*]/, e.target.value) && enteredData.num1 !== '') {
			enteredData.sign = e.target.value;
		} else if (validate(/[\d\.]/, e.target.value) && enteredData.num1 !== '' && enteredData.sign !== '') {
			enteredData.num2 += e.target.value;
			show(enteredData.num2);
			equal.disabled = false;
		} else if (validate(/=/, e.target.value) && enteredData.num1 !== '' && enteredData.sign !== '' && enteredData.num2 !== '') {
			show(res(enteredData.num1, enteredData.sign, enteredData.num2));
			enteredData.num1 = res(enteredData.num1, enteredData.sign, enteredData.num2);
			enteredData.num2 = '';
			enteredData.sign = '';
			equal.disabled = true;
		} else if (e.target.value === 'C') {
			enteredData.num1 = '';
			enteredData.num2 = '';
			enteredData.sign = '';
			display.value = '';
		}
	})

})

