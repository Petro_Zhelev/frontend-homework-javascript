/*Вам потрібно написати програму, яка буде працювати з подіями. 
Ваша програма повинна відслідковувати події кліків на сторінці та зберігати координати місця, де було здійснено клік.
 Після того, як буде здійснено 10 кліків, програма повинна показати користувачу список всіх координат, де були здійснені кліки.

Для виконання цієї задачі вам потрібно використати обробники подій. 
Ви можете використати метод addEventListener() для відслідковування подій кліків миші на сторінці.
 Після кожного кліку на сторінці, вам потрібно зберігати координати місця, де було здійснено клік, у масив. 
 Якщо кількість кліків досягне 10, вам потрібно вивести на сторінку список всіх координат з масиву.*/
let clickCoordinates = [];
const clickField = document.createElement('div');
clickField.classList.add('click-fied');
document.body.append(clickField);
const coordinatesList = document.createElement('div');
      coordinatesList.classList.add('click-list');

// таргет натиску (корисно для користувачів тачпадів, бо не завжди зрозуміло чи спрацював тачпад і відбулася подія, чи ні)

let countClick = 0;
clickField.addEventListener('click', (e) => {
countClick ++;
	const targetClick = document.createElement('div');
      targetClick.classList.add('target');
     targetClick.style.display = 'flex';
	 targetClick.style.top = `${e.offsetY - 15}px`;
	 targetClick.style.left = `${e.offsetX - 15}px`;
	 targetClick.innerText = countClick;
	 clickField.append(targetClick);
	let singleClick = `Координати кліку: X: ${e.offsetX} Y: ${e.offsetY}`;
	clickCoordinates.push(singleClick);
	console.log(clickCoordinates);
	if (clickCoordinates.length === 10) {
		coordinatesList.classList.add('show');
		coordinatesList.innerHTML = `Крайні 10 кліків було тицьнуто по наступних координатах відповідно: 
	${clickCoordinates.map((click, i) => {
			return `<br/> ${(i + 1)}. ${click} `
		})}`

		document.body.append(coordinatesList);
	}
})

coordinatesList.addEventListener('click', () => {
	countClick = 0;
	let [...targets] = document.querySelectorAll('.target');
	targets.forEach((target) => {
		target.remove();
	});
	clickCoordinates = [];
	coordinatesList.classList.remove('show');
})
