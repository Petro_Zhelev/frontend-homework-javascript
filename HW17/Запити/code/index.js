/*Вам потрібно написати програму, яка буде отримувати інформацію з сервера за допомогою fetch запиту, а потім виводити цю інформацію на сторінку. Для цього вам потрібно скласти запит до API за посиланням https://jsonplaceholder.typicode.com/users, який поверне список користувачів у форматі JSON.

Після того, як ви отримаєте відповідь з сервера, вам потрібно перетворити JSON-об'єкт на масив об'єктів за допомогою методу json(), який є частиною об'єкта Response. Далі вам потрібно використати цей масив для створення HTML-елементів, які будуть містити інформацію про кожного користувача.

Наприклад, ви можете створити таблицю з наступними стовпцями: ID користувача, ім'я, email і номер телефону. Для створення таблиці ви можете використати HTML-елементи <table>, <thead>, <tbody>, <tr> і <td>.

Крім того, вам потрібно додати обробник помилок для вашого fetch запиту. Якщо запит не вдається, програма повинна вивести на сторінку повідомлення про помилку.
 */

async function req(url) {
	document.querySelector(".box_loader").classList.add("show")
	const data = await fetch(url);
	if (!data.ok) {
		document.querySelector(".box_loader").classList.remove("show");
		const err = document.createElement('div');
		err.classList.add('err');
		err.innerText = `Сталася помилка`;
		document.body.prepend(err);
	}
	return await data.json();
}
req("https://jsonplaceholder.typicode.com/users4")
	.then((info) => {
		console.log(info);
		show(info);
	})

function show(data = []) {
	const tbody = document.querySelector("tbody");
	tbody.innerHTML = "";
	console.log(data);
	if (!Array.isArray(data)) return;
	data.forEach(({ id, name, email, phone }) => {
		tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${email}</td>
        <td>${phone}</td>
        </tr>
        `)
	})

	document.querySelector(".box_loader").classList.remove("show")
}
