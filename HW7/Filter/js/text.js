function filterBy(arr, dataType){
let needArr = arr.filter((item) => typeof(item) !== dataType);
console.log(needArr);
}
console.log(`Всі дії відбуваються виключно стосовно першого рівня вкладеності об'єкта:
[1, 2, [24, 'hello'], '5', 'null', "hello", null, undefined]
Позбавляемося об'єктів:`);
filterBy([1, 2, [24, 'hello'], '5', 'null', "hello", null, undefined], 'object');
console.log("Позбавляемося undefined:");
filterBy([1, 2, [24, 'hello'], '5', 'null', "hello", null, undefined], 'undefined');
console.log("Позбавляемося строк:");
filterBy([1, 2, [24, 'hello'], '5', 'null', "hello", null, undefined], 'string');
console.log("Позбавляемося чисел:");
filterBy([1, 2, [24, 'hello'], '5', 'null', "hello", null, undefined], 'number');
