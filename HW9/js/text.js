import elementCreator from './functions.js'

window.onload = () =>{

let bodyText = elementCreator('div', 'body-name', 'BODY');
document.body.prepend(bodyText);

let main = elementCreator('div', 'main');
bodyText.after(main);

// header

let header =  elementCreator('header', 'parent', 'HEADER');
header.classList.add('header')
main.prepend(header);

let nav = elementCreator('div', 'child', 'NAV');
header.append(nav);


// section
let section = elementCreator('section', 'parent', 'SECTION');
section.classList.add('.section-left');
header.after(section);

let secHeader = elementCreator('div', 'child', 'HEADER');
section.append(secHeader);

// section>article #2
let article = elementCreator('div', 'child', 'ARTICLE');
section.append(article);

let artHeader = elementCreator('div', 'grand-child', 'HEADER');
article.append(artHeader);

let p1 = elementCreator('p', 'grand-child', 'P');
article.append(p1);

// p2 holder


let pAsideHolder = elementCreator('div', 'p-holder', '');
p1.after(pAsideHolder);

let p2 = elementCreator('p', 'grand-child', 'P');
p2.classList.add("p2");
pAsideHolder.append(p2);

let aside = elementCreator('p', 'grand-child', 'ASIDE');
aside.classList.add('aside');
pAsideHolder.append(aside);

// article>footer

let artFooter = elementCreator('footer', 'grand-child', 'FOOTER');
article.append(artFooter);

// article bottom

let artBottom = elementCreator('div', 'child', 'ARTICLE');
article.after(artBottom)


let artHeaderBottom = elementCreator('div', 'grand-child', 'HEADER');
artBottom.append(artHeaderBottom);

let p2Bottom = elementCreator('p', 'grand-child', 'P');
p2Bottom.classList.add("p");
artHeaderBottom.after(p2Bottom);

let p3Bottom = elementCreator('p', 'grand-child', 'P');
p3Bottom.classList.add("p");
artHeaderBottom.after(p3Bottom);

let artFooterBottom = elementCreator('footer', 'grand-child', 'FOOTER');
artBottom.append(artFooterBottom);
// section>footer

let sectionFooter = elementCreator('footer', 'child', 'FOOTER');
section.append(sectionFooter);

// section right

let sectionRight = elementCreator('section', 'parent', 'SECTION');
sectionRight.classList.add('section-right');
header.after(sectionRight);

let rightHeader = elementCreator('div', 'child', 'HEADER');
rightHeader.classList.add('right-header');
sectionRight.append(rightHeader);

let rightNav = elementCreator('div', 'child', 'NAV');
rightNav.classList.add('nav-right');
sectionRight.append(rightNav);

// footer

let footer = elementCreator('footer', 'parent', 'FOOTER');
footer.classList.add('footer');
main.append(footer);
}

