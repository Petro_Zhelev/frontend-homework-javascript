import { userSlectTopping } from "./functions.js"
import { validate, get } from './functions.js'
import { pizzaSelectUser, userInfo } from "./index.js";
import pizza from "./pizza.js";

export function clickInputSize(e) {
	if (e.target.tagName === "INPUT") {
		userSlectTopping(e.target.value)
	}
}

const choosenToping = document.getElementById('topping');
const choosenSauce = document.getElementById('sauce');
const [...inputs] = document.querySelectorAll('#info input');
const nameInput = get('userName');
const phoneInput = get('userPhone');
const emailInput = get('userEmail');
const submitBtn = document.getElementById('btnSubmit');
let isNameTrue = false, isPhoneTrue = false, isEmailTrue = false;

//  прибираємо пожливість додавання соусів і топінгів по кліку

export const clickToppingAdd = (e) => {
	// 	if (e.target.tagName === "IMG") {
	// 		userSlectTopping(e.target.id);
	// 		choosenToping.innerHTML += `${pizzaSelectUser.topping[pizzaSelectUser.topping.length - 1].productName} <br/>`;
	// 	}
}

export const clickSauceAdd = (e) => {
	// 	if (e.target.tagName === "IMG") {
	// 		userSlectTopping(e.target.id);
	// 		choosenSauce.innerHTML = pizzaSelectUser.sauce.productName;
	// 	}
}

// валідатор форми

inputs.forEach((input) => {
	input.addEventListener('change', (e) => {
		if (e.target.id === 'userName' && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)) {
			userInfo.name = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isNameTrue = true;
		} else if (e.target.id === 'userName' && !validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'userPhone' && validate(/^\+380\d{9}$/ig, e.target.value)) {
			userInfo.phone = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isPhoneTrue = true;
		} else if (e.target.id === 'userPhone' && !validate(/^\+380\d{9}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'userEmail' && validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)) {
			e.target.className = '';
			userInfo.email = e.target.value;
			e.target.classList.add('success');
			isEmailTrue = true;
		} else if (e.target.id === 'userEmail' && !validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		}
	});
	input.addEventListener('click', (e) => {
		if (e.target.type === 'reset') {
			userInfo.name = '';
			userInfo.phone = '';
			userInfo.email = '';
			nameInput.className = '';
			phoneInput.className = '';
			emailInput.className = '';
		}
	}
	)
})
// кінець валідатора форми

submitBtn.addEventListener('click', (e) => {
	console.log(nameInput.value, phoneInput.value, emailInput.value);

	if (isNameTrue = true, isPhoneTrue = true, isEmailTrue = true) {
		window.location.href = "../thank-you/index.html";
	}
})

const ingridients = document.querySelector('.ingridients');
let tableImg = document.querySelector(".table");

// drag and drop початок

ingridients.addEventListener('dragstart', (evt) => {
	evt.target.style.border = '3px dotted grey';
	evt.target.classList.add('selected');
	evt.dataTransfer.effectAllowed = "move";
	evt.dataTransfer.setData("Text", evt.target.id);
}, false);
ingridients.addEventListener("dragend", (evt) => {
	evt.target.style.border = '';
	evt.target.classList.remove('selected');

}, false);

tableImg.addEventListener("dragenter", (evt) => {
	evt.target.style.border = "3px solid red";
}, false);

tableImg.addEventListener("dragover", (evt) => {
	if (evt.preventDefault) evt.preventDefault();
	return false;
}, false);

tableImg.addEventListener("dragleave", (evt) => {
	evt.target.style.border = "";
}, false);

tableImg.addEventListener("drop", (evt) => {

	if (evt.preventDefault) evt.preventDefault();
	if (evt.stopPropagation) evt.stopPropagation();
	evt.target.style.border = "";

	let id = evt.dataTransfer.getData("Text");

	let elem = document.getElementById(id);
	userSlectTopping(elem.id);

	const topingClone = document.createElement("img");
	topingClone.setAttribute('src', document.querySelector('.selected').src);
	topingClone.setAttribute('id', document.querySelector('.selected').id);
	const layers = [...document.querySelectorAll('.table  img')];

	if (topingClone.id.includes('sauce') && layers.find((img) => { return img.id.includes('sauce') })) {
		layers.find((img) => { return img.id.includes('sauce') }).remove();
		document.querySelector('.table').append(topingClone);
		layers.splice(1, 1, topingClone);
		topingClone.style.zIndex = '1';
	} else if (topingClone.id.includes('sauce') && layers.find((img) => { return img.id.includes('sauce') }) === undefined) {
		console.log("works");
		document.querySelector('.table').append(topingClone);
		topingClone.style.zIndex = '1';
		layers[1] = topingClone;
	} else if (!topingClone.id.includes('sauce') && layers.find((img) => { return img.id.includes('sauce') })) {
		document.querySelector('.table').append(topingClone);
		layers.push(topingClone);
		topingClone.style.zIndex = layers.indexOf(topingClone);
	} else {
		console.log('no sauce');
		document.querySelector('.table').append(topingClone);
		layers.push(topingClone);
		topingClone.style.zIndex = layers.length;

	}

	if (topingClone.id.includes('sauce')) {
		choosenSauce.innerHTML = `<br/><span>${pizzaSelectUser.sauce.productName}</span><span class ='x'>X</span>`;
	} else if (!topingClone.id.includes('sauce')) {
		choosenToping.innerHTML += `<span>${pizzaSelectUser.topping[pizzaSelectUser.topping.length - 1].productName}</span><span class ='x'>X</span> <br/>`;
	}

	
	return false;
}, false);

const priceOutput = document.getElementById('price');
	const resultSauce = document.querySelector('.result');
	resultSauce.addEventListener('click', (e) => {
		// debugger

		console.log(pizzaSelectUser);

		let deletedTopingName = e.target.previousSibling.textContent;
		let [...topImg] = document.querySelectorAll('.table img'); 

		// якщо видаляємо соус
		if (pizzaSelectUser.sauce.productName === deletedTopingName) {
			pizzaSelectUser.price -= pizzaSelectUser.sauce.price;
			pizzaSelectUser.sauce = ''; 
			priceOutput.textContent = pizzaSelectUser.price;
			pizza.sauce.forEach((sauce) =>{
				if(deletedTopingName === sauce.productName){
					topImg.forEach((img) => {
						if(img.id === sauce.name){
							img.remove();
						}
					}) 
				}
			})
			console.log(topImg);
			e.target.previousElementSibling.remove();

			e.target.remove();
		}
		// якщо видаляємо топпінг
		pizzaSelectUser.topping.forEach((top, i) => {
			// debugger
			if (top.productName === deletedTopingName) {
				pizzaSelectUser.price -= top.price;
				priceOutput.textContent = pizzaSelectUser.price;
				// top = '';
				pizzaSelectUser.topping.splice(i, 1);
				pizza.topping.forEach((top) =>{
					if(deletedTopingName === top.productName){
						topImg.forEach((img) => {
							if(img.id === top.name){
								img.remove();
							}
						}) 
					}
				})
				console.log(pizzaSelectUser);

				e.target.previousElementSibling.remove();
				e.target.nextElementSibling.remove();

				e.target.remove();			
			}
		})
	})