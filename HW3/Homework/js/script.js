// Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.
let array1 = ['a', 'b', 'c'];
let array2 = [1, 2, 3];
console.log(array1.concat(array2));
// // Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.
array1.push(1, 2, 3);
console.log(array1);

// Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].
console.log(array2.reverse());

// Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.
array2 = [1, 2, 3];
array2.push(4, 5, 6);
console.log(array2);

// Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.
array2 = [1, 2, 3];
array2.unshift(4, 5, 6);
console.log(array2);

// Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.
let array3 = ['js', 'css', 'jq'];
console.log(array3[0]);
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].
let array4 = [1, 2, 3, 4, 5];
console.log(array4.slice(0,3));
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5].
array4.splice(1, 2);
console.log(array4);
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].
array4 = [1, 2, 3, 4, 5];
array4.splice(2, 0, 10);
console.log(array4);
// Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.
let array5 = [3, 4, 1, 2, 7];
console.log(array5.sort());

// Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'.
let arrayHello = ['Привіт, ', 'світ', '!'];
arrayHello.splice(1, 1, 'мир');
console.log(arrayHello.join(""));
// Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').
arrayHello = ['Привіт, ', 'світ', '!'];
arrayHello.splice(0, 1, 'Поки, ');
console.log(arrayHello.join(""));
// Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.
let array6 =[1, 2, 3, 4, 5];
let array7 = new Array(1, 2, 3, 4, 5);
console.log(array6, array7)
// Дан багатовимірний масив arr:
// var arr = {
// 	'ru':['блакитний', 'червоний', 'зелений'],
// 	'en':['blue', 'red', 'green'],
// };
// Виведіть за його допомогою слово 'блакитний' 'blue' .
var arr = {
		'ru':['блакитний', 'червоний', 'зелений'],
		'en':['blue', 'red', 'green'],
	};
console.log(arr['ru'][0], arr['en'][0]);
// Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.
let array8 = ['a', 'b', 'c', 'd'];
console.log(`${array8[0]}+${array8[1]}, ${array8[2]}+${array8[3]} `);
// Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач
//         створіть масив на ту кількість елементів, яку передав користувач.
//         у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.
 let message = prompt('Введіть додатне число', '');
 let usersArray = [];
 console.log(message);
if (message < 0) {
   if (!alert("Ви ввели від'ємне число, будь ласка, натисніть ОК, та введіть додатне число після перезагрузки.")){
window.location.reload();
   }
} else if (isNaN(message) || message === " "){
	if(!alert("Ви ввели неправильне значення. Натисніть ОК і введіть додатне число після перезагрузки")){
		window.location.reload();
	}
} else if (!message.valueOf()){
	if(!alert('Ви ввели порожню строку. Натисніть ОК і введіть додатне число після презагрузки')){
		window.location.reload();
	}
}
 else{
	for (let i = 0; i < message; i++) {
		usersArray.push(i)
 }
}
console.log(typeof message);
 console.log(usersArray);
// Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.
let odd = [];
let pair = [];
for (let y = 0; y <usersArray.length; y++) {
if (y%2) {
	odd.push(y);
} else {
	pair.push(y);
}
}

let par = document.createElement('p');
par.className = 'oddNum';
par.innerHTML = `Непарні числа з проміжку від 0 до ${message} (невключно) : ${odd}`;
document.body.append(par);

let span = document.createElement('span');
span.className = 'pairNum';
span.innerHTML = `Парні числа з проміжку від 0 до ${message}(невключно) : ${pair}`;
document.body.append(span);
