// дві функції нижче не дуже зекономили час і місце в файлі коду, вони зроблені просто щоб попрактикуватися))
export const createTag = (tagName) => {
	return document.createElement(tagName)
}

export const addClass = (element, className) => {
	return element.classList.add(className);
}

// нижче йдуть функції, що дійсно спростили життя))
export const createInputField = (fieldName, inputElement, parentElement) => {
	let holder = document.createElement('div');
	let holderLabel = document.createElement('label');
	holderLabel.textContent = fieldName;
	if (fieldName.includes(' ')) {
		let arrFieldName = fieldName.split('');
		let newFieldName = arrFieldName.filter((e) => { return e !== ' ' });
		fieldName = newFieldName.join('');
	}
	holder.classList.add(fieldName);
	let holderInput = document.createElement('input');
	holderInput.setAttribute('id', inputElement);
	holderLabel.setAttribute('for', holderInput.id);
	parentElement.append(holder);
	holder.append(holderLabel);
	holder.append(document.createElement('br'));
	holder.append(holderInput);
} 

export const validate = (p, v) => p.test(v);