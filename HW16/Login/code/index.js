import { createTag, addClass, createInputField} from './functions.js';
const loginImgHolder = createTag('div');
addClass(loginImgHolder, 'img-holder');
document.body.append(loginImgHolder);

const loginFormHolder = createTag('div');
addClass(loginFormHolder, 'form-holder');
document.body.append(loginFormHolder);

const loginHeader = createTag('h2');
loginHeader.innerText = 'Login';
addClass(loginHeader, 'login-header');
loginFormHolder.append(loginHeader);

const loginForm = createTag('form');
loginFormHolder.append(loginForm);

createInputField('email', 'emailInput', loginForm);
createInputField('password', 'passwordInput', loginForm);


const regHolder = createTag('div');
addClass(regHolder, 'reg-holder');
loginForm.append(regHolder);


let href = '../../Registration form/index.html';
const regQuestion = createTag('span');
regQuestion.innerHTML = `Not a user? <a href= "${href}">Register now</a>`;
regHolder.append(regQuestion);

const loginBtn = createTag('div');
addClass(loginBtn, 'login-btn');
loginBtn.innerText = 'LOGIN';
loginForm.append(loginBtn)

export let loginUser = JSON.parse(localStorage.user);

const imailInput = document.getElementById('emailInput');
const passInput = document.getElementById('passwordInput');

loginBtn.addEventListener('click', (e)=>{
	if(imailInput.value === loginUser.email && passInput.value === loginUser.password){
		window.location.href = '../../Destination/index.html';
}
})

loginForm.addEventListener('change', (e) => {
		if(e.target.id === 'emailInput' && e.target.value === JSON.parse(localStorage.user).email){
			e.target.classList.add('success');
		} else if(e.target.id === 'passwordInput' && e.target.value === JSON.parse(localStorage.user).password) {
			e.target.classList.add('success');	
		} else {
			e.target.classList.add('error');
		}
	})
