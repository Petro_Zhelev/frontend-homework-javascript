import { createTag, addClass, createInputField, validate} from '../../Login/code/functions.js'
import { userInfo } from '../../Login/code/userInfo.js';
const regImgHolder = createTag('div');
addClass(regImgHolder, 'img-holder');
addClass(regImgHolder, 'reg-img');
document.body.append(regImgHolder);

const regFormHolder = createTag('div');
addClass(regFormHolder, 'form-holder');
addClass(regFormHolder, 'reg-form')
document.body.append(regFormHolder);

const regHeader = createTag('h2');
regHeader.innerText = 'Registration form';
addClass(regHeader, 'login-header');
addClass(regHeader, 'reg-header');

regFormHolder.append(regHeader);

const regForm = createTag('form');
regFormHolder.append(regForm);

createInputField('name', 'nameInput', regForm);
createInputField('date of birth', 'birthDateInput', regForm);
const birthDate = document.getElementById('birthDateInput');
birthDate.setAttribute('placeholder', 'dd.mm.yyyy');
createInputField("father's/mother's name", 'parentNameInput', regForm);
createInputField('email', 'emailInput', regForm);
createInputField('mobile No', 'mobileInput', regForm);
const mobile = document.getElementById('mobileInput');
mobile.setAttribute('placeholder', '+380XXXXXXXXX');
createInputField('password', 'passwordInput', regForm);
createInputField('re-password', 'rePasswordInput', regForm);
createInputField('home number', 'homeNumberInput', regForm);
const regBtn = createTag('div');
addClass(regBtn, 'login-btn');
addClass(regBtn, 'reg-btn');
regBtn.innerText = 'SUBMIT';
regForm.append(regBtn);

const [...inputs] = document.querySelectorAll('form input');
let isNameTrue = false, isBirthTrue = false, isParentTrue = false, isEmailTrue = false, isMobile = false,
 isPassword = false, isRePassword = false, isHomePhone = false;

inputs.forEach((input) => {
	input.addEventListener('change', (e) => {
		if (e.target.id === 'nameInput' && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)) {
			userInfo.name = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isNameTrue = true;
		} else if (e.target.id === 'nameInput' && !validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'birthDateInput' && validate(/^(0?\d|[12]\d|3[01])\.(0?\d|1[012])\.((19|20)\d\d)$/ig, e.target.value)) {
			userInfo.birthDate = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isBirthTrue = true;
		} else if (e.target.id === 'birthDateInput' && !validate(/^(0?\d|[12]\d|3[01])\.(0?\d|1[012])\.((19|20)\d\d)$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'parentNameInput' && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)) {
			userInfo.parentName = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isParentTrue = true;
		} else if (e.target.id === 'parentNameInput' && !validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'emailInput' && validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)) {
			userInfo.email = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isEmailTrue = true;
		} else if (e.target.id === 'emailInput' && !validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'mobileInput' && validate(/^\+380\d{9}$/ig, e.target.value)) {
			userInfo.mobile = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isMobile = true;
		} else if (e.target.id === 'mobileInput' && !validate(/^\+380\d{9}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'passwordInput' && validate(/^[a-z\d._!@#$%^&*()=+\-:;]{8,}$/ig, e.target.value)) {
			userInfo.password = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isPassword = true;
		} else if (e.target.id === 'passwordInput' && !validate(/^[a-z\d._!@#$%^&*()=+\-:;]{8,}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'rePasswordInput' && e.target.value === document.getElementById('passwordInput').value) {
			e.target.className = '';
			e.target.classList.add('success');
			isRePassword = true;
		} else if (e.target.id === 'rePasswordInput' && e.target.value !== document.getElementById('passwordInput').value) {
			e.target.className = '';
			e.target.classList.add('error');
		} else if (e.target.id === 'homeNumberInput' && validate(/^\d{5,}$/ig, e.target.value)) {
			userInfo.homeNumber = e.target.value;
			e.target.className = '';
			e.target.classList.add('success');
			isHomePhone = true;
		} else if (e.target.id === 'homeNumberInput' && !validate(/^\d{5,}$/ig, e.target.value)) {
			e.target.className = '';
			e.target.classList.add('error');
		}
	})
	})





		regBtn.addEventListener('click', (e) =>{
			if(isNameTrue === true && isBirthTrue === true && isParentTrue === true && isEmailTrue === true && isMobile === true &&
				isPassword === true && isRePassword === true && isHomePhone === true) {			
			localStorage.user = JSON.stringify(userInfo);
			window.location.href = '../../Destination/index.html';
		}	
	})