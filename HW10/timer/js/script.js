window.onload = () => {
	const get = (tagName, item, idValue) => {
		document.getElementsByTagName(tagName)[item].id = idValue;
	}
// присвоїв id окремим парам нулів і контейнеру лічільника
	const start = get('button', 0, 'start'),
		stop = get('button', 1, 'stop'),
		reset = get('button', 2, 'reset'),
		hh = get('span', 0, 'hh'),
		mm = get('span', 1, 'mm'),
		ss = get('span', 2, 'ss'),
		container = get('div', 0, 'container');

// функція, що спрощує пошук елемента по id

	const getId = (id) => document.getElementById(id);
	
	let ssCounter = 0, mmCounter = 0, hhCounter = 0, flag = false, intervalTimerSs, intervalTimerMm, intervalTimerHh;

// умови вигляду показників таймеру

	let countSs = () => {
		if (ssCounter <10){
			ssCounter = `0${ssCounter}`;
		} else if(ssCounter >9 && ssCounter < 60){
			ssCounter = ssCounter;
		} else if(ssCounter = 60){
			ssCounter = '00';
		}
		getId('ss').innerHTML = ssCounter;
		ssCounter++;
	}

	let countMm = () => {
		if (mmCounter <10){
			mmCounter = `0${mmCounter}`;
		} else if(mmCounter >9 && mmCounter < 60){
			mmCounter = mmCounter;
		} else if(mmCounter = 60){
			mmCounter = '00';
		}
		getId('mm').innerHTML = mmCounter;
		mmCounter++;
	}

	let countHh = () => {
		if (hhCounter <10){
			hhCounter = `0${hhCounter}`;
		} else if(hhCounter >9 && hhCounter < 60){
			hhCounter = hhCounter;
		} else if(hhCounter = 60){
			hhCounter = '00';
		}
		getId('hh').innerHTML = hhCounter;
		hhCounter++;
	}

	//обролюємо натискання кнопок 
	// кольори таймеру

	let changeBackgroundColor = (buttonId, className, removedClass1, removedClass2) => {
		document.getElementById(buttonId).onclick = () => {
			let containerPressed = document.getElementById('container');
			containerPressed.classList.add(className);
			containerPressed.classList.remove(removedClass1, removedClass2);

			// цифри

			if(buttonId === 'start'){
				if(!flag){
					intervalTimerSs = setInterval(countSs, 500);
					intervalTimerMm = setInterval(countMm, 5000);
					intervalTimerHh = setInterval(countHh, 360000);
					flag = true;
				   }
			} else if (buttonId === 'stop'){
				clearInterval(intervalTimerSs);
				clearInterval(intervalTimerMm);
				clearInterval(intervalTimerHh);
				flag = false;
			} else if(buttonId === 'reset') {
				clearInterval(intervalTimerSs);
				ssCounter = 1;
				getId('ss').innerHTML = '00';

				clearInterval(intervalTimerMm);
				mmCounter = 1;
				getId('mm').innerHTML = '00';

				clearInterval(intervalTimerHh);	
				hhCounter = 1; 
				getId('hh').innerHTML = '00';

				flag = false;
			}
		}
	}


	let startClick = changeBackgroundColor('start', 'green', 'red', "silver"),
		stopClick = changeBackgroundColor('stop', 'red', 'green', 'silver'),
		resetClick = changeBackgroundColor('reset', 'silver', 'green', 'red');


}