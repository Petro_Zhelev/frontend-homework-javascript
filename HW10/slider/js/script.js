window.onload = () => {
const slides = document.querySelectorAll('.slide');
let shownSlide = 0;


let nextSlide = () => {
	slides[shownSlide].className = 'slide';
	shownSlide = (shownSlide + 1)%slides.length;
	slides[shownSlide].className = 'slide visible';
}
let slidingInterval = setInterval(nextSlide, 3000);
}