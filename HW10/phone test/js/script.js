const input = document.createElement('input');
input.id = 'input';
input.placeholder ='XXX-XXX-XX-XX';
document.body.prepend(input);
input.focus();

const btn = document.createElement('button');
btn.textContent = 'Save';
input.after(btn);

const error = document.createElement('div');
		error.classList.add('error');
		error.textContent ='Введіть номер телефону за зразком у полі вводу';

let pattern = /^\d\d\d-?\d\d\d-?\d\d-?\d\d$/;


btn.onclick = () => {
	if(pattern.test(input.value)){
		input.classList.add('green');
		setInterval(document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg ', 500);
	} else {
		input.classList.remove('green');
		input.classList.add('red');
		input.value = '';
		input.focus();
		document.body.prepend(error);
	}

}