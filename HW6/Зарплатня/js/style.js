let userName = prompt('Enter your name', "");
let userSurname = prompt('Enter your surname', "");
let userRate = prompt('Enter your rate per one working day', '');
let userDays = prompt('Enter the number of working days', '')

class Worker {
	constructor(name, surname, rate, date) {
		this.name = name,
			this.surname = surname,
			this.rate = rate,
			this.days = date,
			this.getSalary = function () {
				return`${this.name +' '+ this.surname} your rate per ${this.days} days is ${this.rate * this.days}`;
			}
	}
}

const workersSalary = new Worker (userName, userSurname, userRate, userDays);
document.write(workersSalary.getSalary());