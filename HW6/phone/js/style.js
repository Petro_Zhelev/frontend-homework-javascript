class Phone {
	constructor(number, model, weight) {
		this.number = number;
		this.model = model;
		this.weight = weight;
	}
};

let nokia = new Phone("+380959595959", "Nokia", 200);
let xiaomi = new Phone("+380979797979", "Xiaomi", 180);
let iphone = new Phone("+380939393939", "iPhone", 170);

Phone.prototype.showProps = (name) => {
	console.log(name);
}

Phone.prototype.reciveCall = (name) => {
	console.log(`Телефонує ${name}`);
};

Phone.prototype.getNumber = function (name) {
	console.log(name.number);
	return name.number;
};

let nokiaProps = Phone.prototype.showProps(nokia);
let xiaomiProps = Phone.prototype.showProps(xiaomi);
let iphoneProps = Phone.prototype.showProps(iphone);

let callPetro = Phone.prototype.reciveCall('Petro');

let nokiaNumber = Phone.prototype.getNumber(nokia);
let xiaomiNumber = Phone.prototype.getNumber(xiaomi);
let iphoneNumber = Phone.prototype.getNumber(iphone);