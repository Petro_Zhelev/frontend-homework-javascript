class Engine {
	constructor(enginePower, engineManufacturer) {
		this.power = enginePower;
		this.manufact = engineManufacturer;
	}
	toString() {
		return `Power: ${this.power}, Company: ${this.manufact}`;
	}

}


class Driver {
	constructor(fullName, age) {
		this.name = fullName;
		this.experience = age;
	}
	toString = function () {
		return `Driver fullname: ${this.name}, Driver experience: ${this.experience} years`;
	}

}


class Car {
	constructor(carBrand, carWeight, carClass, driver, engine) {
		this.brand = carBrand;
		this.weight = carWeight;
		this.class = carClass;
		this.driver = driver;
		this.engine = engine;
	}
	start() {
		document.getElementById('start').innerHTML = `<p>Поїхали!</p>`;
	}
	stop() {
		document.getElementById('stop').innerHTML = `<p>Зупиняємось.</p>`;

	}
	turnRight() {
		document.getElementById('right').innerHTML = `<p>Поворот праворуч.</p>`;

	}
	turnLeft() {
		document.getElementById('left').innerHTML = `<p>Поворот ліворуч.</p>`;

	}
	toString() {
		return `Brand: ${this.brand}, Class: ${this.class}, Weight: ${this.weight}kg, Driver: ${this.driver}, Engine: ${this.engine}`;
	}
}


const needEngine = new Engine(200, 'bmw');
const needDriver = new Driver('Zhelev Petro', 14);
const needCar = new Car("bmw", 3000, 'sedan', needDriver,needEngine);

console.log(needCar.toString());
needCar.start();
needCar.turnLeft();
needCar.turnRight();
needCar.stop();

class Lorry extends Car {
	constructor(carBrand, carWeight, carClass, driver, engine, carrying) {
		super(carBrand, carWeight, carClass, driver, engine);
		this.carrying = carrying;
	}
	toString() {
		return `Brand: ${this.brand}, Class: ${this.class}, Weight: ${this.weight}kg, Driver: ${this.driver}, Engine: ${this.engine}, Carrying: ${this.carrying}kg`;

	}
}

let lorryDriver = new Driver('Taras Schevchenko', 20);
let lorryEngine = new Engine(3000, 'tgx');
let needLorry = new Lorry('MAN', 25000, 'TGX', lorryDriver, lorryEngine, 30000);
console.log(needLorry.toString());

class SportCar extends Car {
	constructor(carBrand, carWeight, carClass, driver, engine, speed) {
		super(carBrand, carWeight, carClass, driver, engine,);
		this.speed = speed;
	}
	toString() {
		return `Brand: ${this.brand}, Class: ${this.class}, Weight: ${this.weight}kg, Driver: ${this.driver}, Engine: ${this.engine}, Speed: ${this.speed}km/h`
	}
}

let sportCarDriver = new Driver('Ivan Franko', 30);
let sportCarEngine = new Engine(570, 'V10');
let needSportCar = new SportCar('Lamborghini', 2500, 'Gallardo', sportCarDriver, sportCarEngine, 324);
console.log(needSportCar.toString());