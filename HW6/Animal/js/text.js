class Animal{
	constructor(food, location){
this.food = food;
this.location = location;
	}
	makeNoise(){
		return'Така тварина спить.';
	}
	sleep(){

	}
	eat(){
		return'Тут може бути будь-яка їжа';
	}
}

class Dog extends Animal{
	constructor(food, location, isFighting){
		super(food, location);
		this.isFighting = isFighting;
	}
	makeNoise(){
	return'Песики гавкають';
}
    eat(){
		return`Песики подобають ${this.food}`;
	}
}

class Cat extends Animal{
	constructor(food, location, toiletTrained){
		super(food, location);
		this.toiletTrained = toiletTrained; 
	}
	makeNoise(){
		return'Котики кажуть няв';
	}
	eat(){
		return'Котики полюбляють рибку';
	}
}

class Horse extends Animal{
	constructor(food, location, heeled){
		super(food, location);
		this.heeled = heeled;
	}
	makeNoise(){
		return'Коні регочуть';
	}
	eat(){
		return`Коні люблять ${this.food}`;
	}
}

class Vet extends Animal{
	constructor(food, location){
		super(food, location);
		this.food = food;
		this.location = location;
	}
	treatAnimal(){
		return(`Пацієнт їсть ${this.food} живе у ${this.location}`); 
	}
	main(){
		let patients = [];
		patients.push(needDog, needHorse, needCat);
		patients.forEach(element => {
			console.log(`Направлення до ветеринара. Пацієнт прибув з ${element.location}, вживає ${element.food}`);
		});
	}
}

let needAnimal = new Animal('боби', 'Житомир');
console.log(needAnimal.makeNoise());

let needHorse = new Horse('трава', 'Запоріжжя', 'спортивні');
console.log(needHorse.eat());

let needDog = new Dog("м'ясо", 'Дніпро', 'бійцівський');
console.log(needDog.eat());

let needCat = new Cat('риба', 'Київ', 'привчений');

let animalToVet = new Vet(needAnimal.food, needAnimal.location);
console.log(animalToVet.treatAnimal());
animalToVet.main();