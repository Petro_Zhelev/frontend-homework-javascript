class MyString {
	constructor(str){
		this.reverse = (str) =>{
            let strRev = str.split('').reverse().join('');
return strRev;         		
},
	    this.ucFirst = function(str) {
            let strArr = str.split('');
			let strArrFirst = strArr.splice(0,1);
			let strArrFirstUp = strArrFirst.toString().toUpperCase();
			strArr.unshift(strArrFirstUp);
			let strCapit = strArr.join('');
return strCapit;
 },
		this.ucWords = function(str) {
        let strToArr = str.split(' ');
		let capitStr = [];
		strToArr.forEach(element => {
			let capitWord = this.ucFirst(element);
			capitStr.push(capitWord);
			return capitStr;
		});
		let needStr = capitStr.join(' ');
		return needStr;
		}
	}
}

const result = new MyString;

console.log(result.reverse("moloko or kolokol."));
console.log(result.ucFirst("moloko or kolokol."));
console.log(result.ucWords("moloko or kolokol."));