import {city} from "./uacity.js"
/*
Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні. 

Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані 
*/



const log = document.querySelector(".log");
const [...inputs] = document.querySelectorAll("input");
const user = {
    name : "",
    surname: "",
    phone: "",
    email: "",
    city: ""
}

const validate = (p, v) => p.test(v);

inputs.forEach((input)=>{
    /*input.addEventListener("focus", () => {
        //натиск у input
        input.className = ""
        input.classList.add("focus");
    })

    input.addEventListener("blur", () => {
        input.className = ""
        input.classList.add("blur")
    })

    input.addEventListener("change", () => {
        input.className = ""
        input.classList.add("change")
        log.innerHTML = input.value
    })

    input.addEventListener("input", () => {
        log.innerHTML = input.value
    })*/

	const get = (id, displ) =>{
		return (document.getElementById(id).style.display = displ);
	}

    input.addEventListener("change", (e) => {
        if(e.target.dataset.type === "name" && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
            user.name = e.target.value;
            // e.target.classList.add("valid");
			get('well-name', 'block');
			get('wrong-name', 'none');
        }else if(e.target.dataset.type === "name" && !validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
			get('wrong-name', 'block');
			get('well-name', 'none');
		}else if (e.target.dataset.type === "surname" && validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
            user.surname = e.target.value
            // e.target.classList.add("valid");
			get('wrong-surname', 'none');
			get('well-surname', 'block');
		}else if (e.target.dataset.type === "surname" && !validate(/^[а-яієґї'-]{2,}$/ig, e.target.value)){
			get('wrong-surname', 'block');
			get('well-surname', 'none');
        }else if(e.target.dataset.type === "phone" && validate(/^\+380\d{2} ?\d{3} ?\d{2} ?\d{2}$/ig, e.target.value)){
            user.phone = e.target.value
            // e.target.classList.add("valid");
			get('wrong-phone', 'none');
			get('well-phone', 'block');
		}else if(e.target.dataset.type === "phone" && !validate(/^\+380\d{2} \d{3} \d{2} \d{2}$/ig, e.target.value)){
			get('wrong-phone', 'block');
			get('well-phone', 'none');
		}else if(e.target.dataset.type === "email" && validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)){
            user.email = e.target.value
            // e.target.classList.add("valid");
			get('wrong-email', 'none');
			get('well-email', 'block');
		}else if(e.target.dataset.type === "email" && !validate(/^[a-z_.0-9]+@[a-z0-9-.]+\.[a-z.]+$/ig, e.target.value)){
			get('wrong-email', 'block');
			get('well-email', 'none');
        }else{
            e.target.className = ""
            // e.target.classList.add("error");
        }
    })
})

city.forEach((c)=>{
    const datalist = document.querySelector("datalist");
    const option = document.createElement("option");
    option.value = c.city;
    datalist.append(option)
})

// log.innerHTML = city;
