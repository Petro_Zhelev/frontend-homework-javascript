
/*
1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"
focus blur input

2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість,
 то межа інпуту стає зеленою, якщо неправильна – червоною.
*/


window.addEventListener('DOMContentLoaded', () =>{
  const [...inputs] = document.querySelectorAll('input');
  inputs.forEach((input) => {
	input.addEventListener('blur', () => {
		console.log(input.value.length);
		console.log(input.dataset.length)
		if(input.value.length === parseInt(input.dataset.length)) {
			input.className = '';
			input.classList.add('valid')
		} else {
			input.className = '';
			input.classList.add('error')
		}
		document.getElementById('test').innerHTML += input.value + "<br/>";
		// document.getElementById('test').innerHTML = input.value; варіант, якщо потрібні не всі значення інпутів, а тільки останне введене
	})
  })


})

/*3. 
Використовуючи бібліотеку bootstrap створіть форму у якій запитати у користувача данні.
Ім'я, Прізвище (Українською)
Список з містами України 
Номер телефону у форматі +380XX XXX XX XX - Визначити код оператора та підтягувати логотип оператора. 
Пошта 
Якщо поле має помилку показати червоний хрестик  біля поля ❌,  якщо помилки немає показати зелену галочку ✅

Перевіряти данні на етапі втрати фокуса та коли йде натискання кнопки відправити дані */
