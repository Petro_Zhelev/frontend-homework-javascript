/*- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

window.addEventListener('DOMContentLoaded', () =>{
	const input = document.querySelector('input');
	const span = document.querySelector('span');
	const button = document.querySelector('button');
	const error = document.getElementById('error-message');
	input.addEventListener('focus', () => {
		input.className = '';
		input.classList.add('focus');
		error.style.display = 'none';

	});
	input.addEventListener('blur', () => {
		input.className = "";
		if(parseInt(input.value) < 0){
			input.className = "";
			input.classList.add('error');
			error.style.display = 'block';
span.style.display = 'none';
button.style.display = 'none';

error.innerText = `Please enter correct price`;
return;
		} else{
		span.innerText = 'Текст який мав тут бути, але в умові залишилася лише крапка, тож "."'
		span.style.display = 'inline-block';
		button.style.display = 'inline-block';
		}
		
	});
	button.addEventListener('click', ()=>{
		button.style.display = "none";
		span.style.display = 'none';
	})
})