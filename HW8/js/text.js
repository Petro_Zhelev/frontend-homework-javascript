window.onload = () => {

	let btn = document.querySelector('#frst-btn')
	btn.onclick = function () {
		// створюю поле вводу
		const diametrSet = document.createElement('input');
		diametrSet.classList.add('diametr-set');
		diametrSet.setAttribute('type', 'number');
		diametrSet.setAttribute('placeholder', 'Введіть діаметр кола');
		document.body.append(diametrSet);
		diametrSet.focus();
		// створюю кнопку для обробки даних поля вводу
		const createBtn = document.createElement('input');
		createBtn.setAttribute('type', 'button');
		createBtn.setAttribute('value', 'Намалювати');
		document.body.append(createBtn);
		const hr = document.createElement('hr');
		document.body.append(hr);

		// функція розмноження кіл
	createBtn.onclick = function(){ 
		for (let i = 0; i < 100; i++) {
		   createCircles();
		}
		// deleteCircle();
	};
let circleContainer = document.createElement('div');
circleContainer.style.display = 'grid';
circleContainer.style.grid = 'repeat(10, 1fr )/ repeat(10, 1fr)';
circleContainer.style.gap = '10px';
circleContainer.style.justifyItems = 'center';
document.body.append(circleContainer);

	
	// функція для створення кола	
	let createCircles = () => {
		const circle = document.createElement('div')
		circle.classList.add('circle');
		circle.style.width = diametrSet.value + 'px';
		circle.style.height = diametrSet.value + 'px';
		circle.style.border = "2px solid black";
		circle.style.borderRadius = '50%';
		circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
		circleContainer.append(circle);
		circle.onclick = () => {
			circle.remove()
		}
	};
	
	}		

}