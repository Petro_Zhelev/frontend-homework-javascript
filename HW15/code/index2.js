const links = {
	nbu: "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json",
	star: [
		"https://swapi.dev/api/planets",
		"https://swapi.dev/api/planets/?page=2",
		"https://swapi.dev/api/planets/?page=3",
		"https://swapi.dev/api/planets/?page=4",
		"https://swapi.dev/api/planets/?page=5",
		"https://swapi.dev/api/planets/?page=6"
	],
	todo: "https://jsonplaceholder.typicode.com/todos"
};

let swich = false;

const req = async (url) => {
	document.querySelector(".box_loader").classList.add("show");
	const data = await fetch(url);
	return await data.json();
};

const nav = document.querySelector(".nav")
 .addEventListener("click", (e) => {
	const link = links[e.target.dataset.link];
	if (link) {
		swich = false;
		if (Array.isArray(link)) {
			Promise.all(link.map(req))
			.then((infos) => {
				const data = infos.reduce((acc, info) => [...acc, ...info.results], []);
				data.forEach((d, i) => {
					d.index = i;
				});
				swich = true;
				show(data);
			});
		} else {
			req(link).then((info) => {
				show(info);
			});
		}
	}
});

function show(data = []) {
	if (!Array.isArray(data)) return;
	const tbody = document.querySelector("tbody");
	if (!swich) {
		tbody.innerHTML = "";
	}
	const newArr = data.map(({ txt, rate, exchangedate, title, completed, name, diameter, population, index }, i) => {
		return {
			id: index + 1 || i + 1,
			name: txt || title || name,
			info1: rate || completed || diameter,
			info2: exchangedate || population || "тут пусто",
		};
	});

	newArr.forEach(({ name, id, info1, info2 }) => {
		tbody.insertAdjacentHTML(
			"beforeend",
			<tr>
				<td>${id}</td>
				<td>${name}</td>
				<td>${info1}</td>
				<td>${info2}</td>
			</tr>
		);
	});

	document.querySelector(".box_loader").classList.remove("show");
}