/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

героїв зоряних війн, 

список справ з https://jsonplaceholder.typicode.com/ 
//виводити які з можливістю редагування при натискані 
*/
let swich = false;

const req = async (url) => {
	document.querySelector(".box_loader").classList.add("show")
	const data = await fetch(url);
	return await data.json();
}

const nav = document.querySelector(".nav")
	.addEventListener("click", (e) => {
		if (e.target.dataset.link === "nbu") {
			swich = false;
			req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
				.then((info) => {
					show(info)
				})
		} else if (e.target.dataset.link === "star") {

			req("https://swapi.dev/api/planets")
				.then((info) => {
					info.results.forEach((r, i) => { r.index = i });
					console.log(info.results);
					req("https://swapi.dev/api/planets/?page=2")
						.then((info) => {
							info.results.forEach((r, i) => { r.index = i + 10 });
							console.log(info.results);
							swich = true;
							req("https://swapi.dev/api/planets/?page=3")
								.then((info) => {
									info.results.forEach((r, i) => { r.index = i + 20 });
									console.log(info.results);
									swich = true;
									req("https://swapi.dev/api/planets/?page=4")
										.then((info) => {
											info.results.forEach((r, i) => { r.index = i + 30 });
											console.log(info.results);
											swich = true;
											req("https://swapi.dev/api/planets/?page=5")
												.then((info) => {
													info.results.forEach((r, i) => { r.index = i + 40 });
													console.log(info.results);
													swich = true;
													req("https://swapi.dev/api/planets/?page=6")
														.then((info) => {
															info.results.forEach((r, i) => { r.index = i + 50 });
															console.log(info.results);
															swich = true;
															show(info.results)
														})
													show(info.results)
												})
											show(info.results)
										})
									show(info.results)
								})
								
							show(info.results)
						})
						
					show(info.results);
				})

			// вивести всі 60 планет з https://swapi.dev/api/planets/
			

		} else if (e.target.dataset.link === "todo") {
			swich = false;
			req("https://jsonplaceholder.typicode.com/todos")
				.then((info) => {
					console.log(info);
					show(info)
				})
		} else {

		}
	})

function show(data = []) {
	if (!Array.isArray(data)) return;
	const tbody = document.querySelector("tbody");
	if(!swich) {tbody.innerHTML = "";}
	const newArr = data.map(({ txt, rate, exchangedate, title, completed, name, diameter, population, index }, i) => {
		return {
			id: index + 1 || i + 1,
			name: txt || title || name,
			info1: rate || completed || diameter,
			info2: exchangedate || population || "тут пусто"
		}
	});

	newArr.forEach(({ name, id, info1, info2 }) => {
		tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `)
	})

	document.querySelector(".box_loader").classList.remove("show")
}